# noinspection PyUnresolvedReferences
from conans import ConanFile

class AxiomCollectionConan(ConanFile):
  name = "axiom-collection"
  version = "0.1.0"
  license = "MIT"
  author = "Sergey A. Erkin (sergey.a.erkin@gmail.com)"
  url = "https://saerkin@bitbucket.org/saerkin/axiom-list.git"
  settings = "os", "compiler", "build_type", "arch"
  exports_sources = "include/*"
  no_copy_source = True
  generators = "cmake"
  requires = \
    "axiom-ptr/0.1.0@sergey-erkin/experimental", \
    "Catch/1.12.2@bincrafters/stable"

  def package(self):
    self.copy("*.h")

  def package_id(self):
    self.info.header_only()
