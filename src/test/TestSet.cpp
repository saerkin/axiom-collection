#include <catch.hpp>
#include <axiom/set.h>
#include <axiom/array.h>

using namespace axiom;

TEST_CASE("empty set has no size", "axiom::set") {
  set<int> s;
  REQUIRE(s.isEmpty());
  REQUIRE(s.size() == 0); // NOLINT(readability-container-size-empty)
}

TEST_CASE("set can be constructed from array", "axiom::set") {
  array<int> a =
    array<int>()
      .add(1)
      .add(2)
      .add(3);

  set<int> s(a);
  REQUIRE(s.size() == a.size());
  REQUIRE(s.has(1));
  REQUIRE(s.has(2));
  REQUIRE(s.has(3));
}

TEST_CASE("set contains added elements", "axiom::set") {
  set<int> s =
    set<int>()
      .add(1)
      .add(2)
      .add(3);

  REQUIRE(s.size() == 3);
  REQUIRE(s.has(1));
  REQUIRE(s.has(2));
  REQUIRE(s.has(3));

  REQUIRE_FALSE(s.has(4));
}

TEST_CASE("set don't duplicate elements", "axiom::set") {
  set<int> s =
    set<int>()
      .add(1)
      .add(1)
      .add(1);

  REQUIRE(s.size() == 1);
  REQUIRE(s.has(1));
}

TEST_CASE("set doesn't contain removed elements", "axiom::set") {
  set<int> s =
    set<int>()
      .add(1)
      .add(2)
      .add(2)
      .add(3);

  s = s.remove(2);

  REQUIRE(s.size() == 2);
  REQUIRE(s.has(1));
  REQUIRE(s.has(3));
  REQUIRE_FALSE(s.has(2));
}

TEST_CASE("set contains all elements from added collection", "axiom::set") {
  set<int> s1 =
    set<int>()
      .add(1)
      .add(2)
      .add(3);

  set<int> s2 =
    set<int>()
      .add(4)
      .add(5)
      .add(5)
      .add(6);

  set<int> s = s1.addAll(s2);

  REQUIRE(s.size() == 3 + 3);
  REQUIRE(s.has(4));
  REQUIRE(s.has(5));
  REQUIRE(s.has(6));
}

TEST_CASE("set is iterable", "axiom::set") {
  set<int> s =
    set<int>()
      .add(1)
      .add(2)
      .add(3)
      .add(4)
      .add(5);

  iterator<int> it = s.begin();
  for (int i = 0; it.hasNext(); i++) {
    REQUIRE(it.next() == i + 1);
  }
}

TEST_CASE("sets are equals only when they have same elements", "axiom::set") {
  set<int> s1 =
    set<int>()
      .add(1)
      .add(2)
      .add(3);

  set<int> s2 =
    set<int>()
      .add(2)
      .add(1)
      .add(3);

  set<int> s3 =
    set<int>()
      .add(1)
      .add(2)
      .add(3)
      .add(4);

  REQUIRE(s1 == s2);
  REQUIRE(s1 != s3);
  REQUIRE(s2 != s3);
}
