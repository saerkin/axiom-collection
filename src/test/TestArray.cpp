#include <catch.hpp>
#include <axiom/set.h>
#include <axiom/array.h>

using namespace axiom;

TEST_CASE("empty array has no size", "axiom::array") {
  array<int> a;
  REQUIRE(a.isEmpty());
  REQUIRE(a.size() == 0); // NOLINT(readability-container-size-empty)
}

TEST_CASE("array should support equals", "axiom::array") {
  array<int> a1 =
    array<int>()
      .add(1)
      .add(2)
      .add(3);

  array<int> a2 =
    array<int>()
      .add(1)
      .add(2)
      .add(3);

  array<int> a3 =
    array<int>()
      .add(4)
      .add(5)
      .add(5)
      .add(6);

  REQUIRE(a1 == a2);
  REQUIRE(a2 != a3);
  REQUIRE(a3 == a3);
}

TEST_CASE("array can be constructed from set", "axiom::srray") {
  set<int> s =
    set<int>()
      .add(1)
      .add(2)
      .add(3);

  array<int> a(s);
  REQUIRE(a.size() == s.size());
  REQUIRE(a[0] == 1);
  REQUIRE(a[1] == 2);
  REQUIRE(a[2] == 3);
}

TEST_CASE("array is iterable", "axiom::array") {
  array<int> a =
    array<int>()
      .add(1)
      .add(2)
      .add(3)
      .add(4)
      .add(5);

  iterator<int> it = a.begin();
  for (int i = 0; it.hasNext(); i++) {
    REQUIRE(it.next() == a[i]);
  }
}

TEST_CASE("array should contain added elements", "axiom::array") {
  array<int> a =
    array<int>()
      .add(1)
      .add(2)
      .add(3);

  REQUIRE(a.size() == 3);
  REQUIRE(a[0] == 1);
  REQUIRE(a[1] == 2);
  REQUIRE(a[2] == 3);

  REQUIRE(a.has(3));
  REQUIRE_FALSE(a.has(4));
}

TEST_CASE("array should duplicate same added element", "axiom::array") {
  array<int> a =
    array<int>()
      .add(1)
      .add(1)
      .add(1);

  REQUIRE(a.size() == 3);
  REQUIRE(a[0] == 1);
  REQUIRE(a[1] == 1);
  REQUIRE(a[2] == 1);
}

TEST_CASE("array on copy should has only copied elements", "axiom::array") {
  array<int> a1 =
    array<int>()
      .add(1)
      .add(2)
      .add(3);

  array<int> a2 =
    array<int>()
      .add(4)
      .add(5)
      .add(6)
      .add(7);

  a1 = a2;

  REQUIRE(a1.size() == 4);
  REQUIRE(a1[0] == 4);
  REQUIRE(a1[1] == 5);
  REQUIRE(a1[2] == 6);
  REQUIRE(a1[3] == 7);
}

TEST_CASE("array should not has removed elements", "axiom::array") {
  array<int> a =
    array<int>()
      .add(1)
      .add(2)
      .add(2)
      .add(3);

  a = a.remove(2);

  REQUIRE(a.size() == 2);
  REQUIRE(a[0] == 1);
  REQUIRE(a[1] == 3);
  REQUIRE_FALSE(a.has(2));
}

TEST_CASE("array should support addAll", "axiom::array") {
  array<int> a1 =
    array<int>()
      .add(1)
      .add(2)
      .add(3);

  array<int> a2 =
    array<int>()
      .add(4)
      .add(5)
      .add(5)
      .add(6);

  array<int> a = a1.addAll(a2);

  REQUIRE(a.size() == 3 + 4);
  REQUIRE(a[0] == 1);
  REQUIRE(a[1] == 2);
  REQUIRE(a[2] == 3);
  REQUIRE(a[3] == 4);
  REQUIRE(a[4] == 5);
  REQUIRE(a[5] == 5);
  REQUIRE(a[6] == 6);
}

TEST_CASE("array should support addAll for itself", "axiom::array") {
  array<int> a1 =
    array<int>()
      .add(1)
      .add(2)
      .add(3);

  array<int> a = a1.addAll(a1);

  REQUIRE(a.size() == 3 + 3);
  REQUIRE(a[0] == 1);
  REQUIRE(a[1] == 2);
  REQUIRE(a[2] == 3);
  REQUIRE(a[3] == 1);
  REQUIRE(a[4] == 2);
  REQUIRE(a[5] == 3);
}