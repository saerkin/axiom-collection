#pragma once

#include <axiom/iterator.h>

namespace axiom {

/**
 *
 */
template<class E>
class collection {
// Constructors
public:
  virtual ~collection() {}

// Methods
public:
  virtual int size() const = 0;

  virtual bool isEmpty() const {
    return 0 == size();
  }

  virtual iterator<E> begin() const = 0;
};

} // end of axiom