#pragma once

#include <axiom/iterator.h>
#include <axiom/collection.h>
#include <axiom/shared_ptr.h>
#include <cassert>
#include <set>

namespace axiom {

/**
 * Immutable set
 */
template<class E>
class set : public collection<E> {
// Fields
private:
  typedef std::set<E> SetData;
  shared_ptr<SetData> p;

// Constructors
public:
  /**
   * Constructs empty set
   */
  inline set() : p(new SetData()) {}

  /**
   * Constructs copy set
   */
  explicit set(const collection<E>& col) : p(new SetData()) {
    int size = col.size();
    if (size > 0) {
      iterator<E> it = col.begin();
      for (int i = 0; it.hasNext() && i < size; i++) {
        p->insert(it.next());
      }
    }
  }

private:
  inline explicit set(const shared_ptr<SetData>& p) : p(p) {}

// Operators
public:
  inline set<E>& operator=(const set<E>& that) {
    if (this != &that) {
      p = that.p;
    }
    return *this;
  }

  inline bool operator==(const set<E>& that) const {
    return this->equals(that);
  }

  inline bool operator!=(const set<E>& that) const {
    return !this->equals(that);
  }

// Methods
public:
  int size() const {
    return p->size();
  }

  iterator<E> begin() const {
    struct SetIterator : public Iterator<E> {
      shared_ptr<SetData> p;
      typename SetData::const_iterator it;

      inline explicit SetIterator(const shared_ptr<SetData>& p)
        : p(p), it(p->begin()) {}

      bool hasNext() const {
        return it != p->end();
      }

      const E& next() {
        assert(it != p->end());
        return *it++;
      }
    };

    return iterator<E>(
      shared_ptr<Iterator<E>>(
        new SetIterator(p)
      ));
  }

  inline bool equals(const set<E>& that) const {
    if (this == &that) {
      return true;
    }

    return *p == *that.p;
  }

  inline bool has(const E& e) const {
    return p->find(e) != p->end();
  }

  inline set<E> add(const E& e) const {
    shared_ptr<SetData> copy(new SetData(*p));
    copy->insert(e);
    return set<E>(copy);
  }

  /**
   * Adds all elements from specified set to this set
   */
  set<E> addAll(const collection<E>& s) const {
    if (s.isEmpty()) {
      return *this;
    }

    shared_ptr<SetData> copy(new SetData(*p));
    iterator<E> it = s.begin();
    while (it.hasNext()) {
      copy->insert(it.next());
    }

    return set<E>(copy);
  }

  /**
   * Removes specified element if this set contains it
   */
  inline set<E> remove(const E& e) const {
    if (this->isEmpty()) {
      return *this;
    }

    shared_ptr<SetData> copy(new SetData(*p));
    copy->erase(e);
    return set<E>(copy);
  }
};

} // end of axiom