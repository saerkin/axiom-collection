#pragma once

#include <axiom/iterator.h>
#include <axiom/collection.h>
#include <axiom/shared_ptr.h>
#include <cassert>

namespace axiom {

/**
 * Immutable array
 */
template<class E>
class array : public collection<E> {
// Nested classes
private:
  struct ArrayData {
    E* elements; // nullable NOLINT(misc-non-private-member-variables-in-classes)
    int size; // NOLINT(misc-non-private-member-variables-in-classes)

    inline ArrayData(E* elements, int size)
      : elements(elements), size(size) {
      assert(size >= 0);
    }

    inline ArrayData() : elements(NULL), size(0) {}

    inline ~ArrayData() {
      delete[] elements;
    }
  };
// Fields
private:
  shared_ptr<ArrayData> p;

// Constructors
public:
  /**
   * Constructs empty array
   */
  inline array() : p(new ArrayData()) {}

  inline explicit array(const E& e) : p(new ArrayData(new E[1], 1)) {
    p->elements[0] = e;
  }

  inline array(const E& e0, const E& e1) : p(new ArrayData(new E[2], 2)) {
    p->elements[0] = e0;
    p->elements[1] = e1;
  }

  inline array(const E& e0, const E& e1, const E& e2) : p(new ArrayData(new E[3], 3)) {
    p->elements[0] = e0;
    p->elements[1] = e1;
    p->elements[2] = e2;
  }

  /**
   * Constructs copy array
   */
  explicit array(const collection<E>& col) : p(new ArrayData()) {
    int size = col.size();
    if (size > 0) {
      p = shared_ptr<ArrayData>(new ArrayData(new E[size], size));
      iterator<E> it = col.begin();
      for (int i = 0; it.hasNext() && i < size; i++) {
        p->elements[i] = it.next();
      }
    }
  }

private:
  inline explicit array(E* elements, int size)
    : p(new ArrayData(elements, size)) {}

// Operators
public:
  inline array<E>& operator=(const array<E>& that) {
    if (this != &that) {
      p = that.p;
    }
    return *this;
  }

  inline bool operator==(const array<E>& that) const {
    return this->equals(that);
  }

  inline bool operator!=(const array<E>& that) const {
    return !this->equals(that);
  }

  inline const E& operator[](int index) const {
    return this->get(index);
  }

// Methods
public:
  int size() const {
    return p->size;
  }

  inline iterator<E> begin() const {
    struct ArrayIterator : public Iterator<E> {
      shared_ptr<ArrayData> p; // NOLINT(misc-non-private-member-variables-in-classes)
      int index; // NOLINT(misc-non-private-member-variables-in-classes)

      inline explicit ArrayIterator(const shared_ptr<ArrayData>& p)
        : p(p), index(0) {}

      bool hasNext() const {
        return index < p->size;
      }

      const E& next() {
        assert(index < p->size);
        return p->elements[index++];
      }
    };

    return iterator<E>(
      shared_ptr<Iterator<E>>(
        new ArrayIterator(p)
      ));
  };

  inline const E& get(int index) const {
    assert(index >= 0);
    assert(index < p->size);
    return p->elements[index];
  }

  bool equals(const array<E>& that) const {
    if (this == &that) {
      return true;
    }

    if (size() != that.size()) {
      return false;
    }

    for (int i = 0; i < size(); i++) {
      if (this->get(i) != that.get(i)) {
        return false;
      }
    }

    return true;
  }

  bool has(const E& e) const {
    for (int i = 0; i < size(); i++) {
      if (get(i) == e) {
        return true;
      }
    }
    return false;
  }

  /**
   * Adds new element to the end of this array.
   * No element' duplication checks
   */
  array<E> add(const E& e) const {
    E* copy = new E[size() + 1];

    int i = 0;
    for (; i < size(); i++) {
      copy[i] = get(i);
    }
    copy[i] = e;

    return array<E>(copy, size() + 1);
  }

  /**
   * Adds all elements from specified array to the end of this array.
   * No element' duplication checks are made
   */
  array<E> addAll(const collection<E>& col) const {
    if (col.isEmpty()) {
      return *this;
    }

    int copySize = size() + col.size();
    E* copy = new E[copySize];

    int i = 0;
    for (; i < size(); i++) {
      copy[i] = get(i);
    }

    iterator<E> it = col.begin();
    for (; it.hasNext() && i < copySize; i++) {
      copy[i] = it.next();
    }

    return array<E>(copy, copySize);
  }

  /**
   * Removes EVERY element which is equal to specified
   */
  array<E> remove(const E& remove) const {
    if (this->isEmpty()) {
      return *this;
    }

    E* copy = new E[size()];

    int i = 0;
    int j = 0;
    for (; i < size(); i++) {
      const E& e = get(i);
      if (e != remove) {
        copy[j++] = e;
      }
    }

    return array<E>(copy, j);
  }
};

} // end of axiom