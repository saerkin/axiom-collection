#pragma once

#include <axiom/shared_ptr.h>

namespace axiom {

/**
 * Iterator interface
 */
template<class E>
struct Iterator {
  virtual bool hasNext() const = 0;

  virtual const E& next() = 0;

  virtual ~Iterator() {}
};

/**
 * Iterator smart ptr
 */
template<class E>
class iterator {
public:
private:
  shared_ptr<Iterator<E>> p;

// Constructors
public:
  inline explicit iterator(const shared_ptr<Iterator<E>>& p) : p(p) {}

  inline iterator(const iterator<E>& that) : p(that.p) {}

// Operators
private:
  iterator<E>& operator=(const iterator<E>& that);

// Methods
public:
  inline bool hasNext() const {
    return p->hasNext();
  }

  inline const E& next() {
    return p->next();
  }
};

} // end of axiom